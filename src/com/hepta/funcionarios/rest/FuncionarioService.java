package com.hepta.funcionarios.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.persistence.FuncionarioDAO;

@Path("/funcionarios")
public class FuncionarioService {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private FuncionarioDAO dao;

    public FuncionarioService() {
        dao = new FuncionarioDAO();
    }

    protected void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Adiciona novo Funcionario
     * 
     * @param funcionario: Novo Funcionario
     * @return response 200 (OK) - Conseguiu adicionar
     */
    @POST
    @Path("/cadastro")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response FuncionarioCreate(Funcionario funcionario) {
        try {
            dao.save(funcionario);     
        }catch(Exception e){
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao cadastrar um funcionario").build();
        }
        return Response.status(200).entity("Cadastro de um funcionarioa realizado com sucesso.").build();
    }

    /**
     * Lista todos os Funcionarios
     * 
     * @return response 200 (OK) - Conseguiu listar
     */
    @GET
    @Path("/listar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response FuncionarioRead() {
    	  List<Funcionario> funcionarios = new ArrayList<>();
          try {
              funcionarios = dao.getAll();
          } catch (Exception e) {
              return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar todos os funcionarios").build();
          }
          GenericEntity<List<Funcionario>> entity = new GenericEntity<List<Funcionario>>(funcionarios) {
          };
          return Response.status(Status.OK).entity(entity).build();
    }
    
    /**
     * Lista um dos Funcionarios por id
     * 
     * @return response 200 (OK) - Conseguiu listar
     */
    @GET
    @Path("/listar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response FuncionarioReadId(@PathParam("id") Integer id) {
    	Funcionario funcionario = new Funcionario();
          try {
            funcionario =  dao.find(id);
          } catch (Exception e) {
              return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar todos os funcionarios").build();
          }
          GenericEntity<Funcionario> entity = new GenericEntity<Funcionario>(funcionario) {
          };
          return Response.status(Status.OK).entity(entity).build();
    }
    
    

    /**
     * Atualiza um Funcionario
     * 
     * @param id:          id do Funcionario
     * @param funcionario: Funcionario atualizado
     * @return response 201 (Created) - Conseguiu atualizar
     */
    @PUT
    @Path("/editar/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response FuncionarioUpdate(@PathParam("id") Integer id, Funcionario funcionario) {
        try{
            dao.update(funcionario, id);
        } catch (Exception e){
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao atualizar os dados de um funcionario").build();
        }
        return Response.status(201).entity("Dados do funcionario atualizados com sucesso.").build();
    }

    /**
     * Remove um Funcionario
     * 
     * @param id: id do Funcionario
     * @return response 200 (OK) - Conseguiu remover
     */
    @DELETE
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response FuncionarioDelete(@PathParam("id") Integer id) {
        try{
            dao.delete(id);
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar um funcionario").build();
        }
        return Response.status(200).entity("Funcionario excluido com sucesso.").build();
    }

    /**
     * Métodos simples apenas para testar o REST
     * @return
     */
    @Path("/teste")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String TesteJersey() {
        return "Funcionando.";
    }

}
