package com.hepta.funcionarios.rest;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/setores")
public class SetorService {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private SetorDAO setorDAO;

    public SetorService(){ setorDAO = new SetorDAO();}

    /**
     * Adiconar novo Setor
     * @param setor: novo Setor
     * @return response 200 (OK) - Conseguiu adicionar
     */
    @POST
    @Path("/cadastro")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response SetorCreate (Setor setor){

        try{
            setorDAO.save(setor);
        }catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Erro ao cadastrar um setor").build();
        }
        return Response.status(200).entity("Cadastro de um setor realizado com sucesso.").build();
    }

    /**
     * Lista todos os Setores
     * @return response 200 (OK) - Conseguiu listar
     */
    @GET
    @Path("/listar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response SetorRead() {
        List<Setor> setores = new ArrayList<>();
        try{
            setores = setorDAO.getAll();
        }catch (Exception e){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar todos os setores").build();
        }

        GenericEntity<List<Setor>> entity = new GenericEntity<List<Setor>>(setores){
        };
        return Response.status(Response.Status.OK).entity(entity).build();
    }
    
    /**
     * Lista um setor por id
     * @return response 200 (OK) - Conseguiu listar
     */
    @GET
    @Path("/listar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response SetorReadById(@PathParam("id") Integer id) {
    	Setor setor = new Setor();
        try{
            setor = setorDAO.find(id);
        }catch (Exception e){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar todos os setores").build();
        }

        GenericEntity<Setor> entity = new GenericEntity<Setor>(setor){
        };
        return Response.status(Response.Status.OK).entity(entity).build();
    }

    /**
     *
     * @param id        id do Setor
     * @param setor     Setor Atualizado
     * @return response 201 (Created) - Conseguiu atualizar
     */
    @PUT
    @Path("editar/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response SetorUpdate(@PathParam("id") Integer id, Setor setor){
        try{
            setorDAO.update(setor, id);
        } catch (Exception e){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Erro ao alterar os dados de um setor").build();
        }
        return Response.status(201).entity("Dados do setor atualizados com sucesso.").build();
    }

    /**
     * Remove um Setor
     * @param id:   id do Setor
     * @return      response 200 (OK) - Conseguiu remover
     */
    @DELETE
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response SetorDelete(@PathParam("id") Integer id) {
        try{
            setorDAO.delete(id);
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar um setor").build();
        }
        return Response.status(200).entity("Setor excluido com sucesso.").build();
    }
}
