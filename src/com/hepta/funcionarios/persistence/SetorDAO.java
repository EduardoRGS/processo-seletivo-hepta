package com.hepta.funcionarios.persistence;

import com.hepta.funcionarios.entity.Setor;

import javax.persistence.EntityManager;
import java.util.List;

public class SetorDAO {

    public void save(Setor setor) throws Exception {
        EntityManager em = HibernateUtil.getEntityManager();

        try{
            em.getTransaction().begin();
            em.persist(setor);
            em.getTransaction().commit();
        }catch (Exception e) {
            em.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            em.close();
        }
    }

    public Setor update(Setor setor, Integer id) throws Exception {
        EntityManager em = HibernateUtil.getEntityManager();

        Setor setorAtualizado = em.find(Setor.class, id);
        setorAtualizado.setNome(setor.getNome());

        try{
            em.getTransaction().begin();
            em.merge(setorAtualizado);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }

        return setorAtualizado;
    }

    public void delete(Integer id) throws Exception {
        EntityManager em = HibernateUtil.getEntityManager();
        try {
            em.getTransaction().begin();
            Setor setor = em.find(Setor.class, id);
            em.remove(setor);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            em.close();
        }
    }

    public List<Setor> getAll() throws Exception{
        EntityManager em = HibernateUtil.getEntityManager();

        try {
            String query = "SELECT s FROM Setor s";
            List<Setor> setores = em.createQuery(query, Setor.class).getResultList();
            return setores;

        }catch (Exception e){
            em.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            em.close();
        }
    }
    
    public Setor find(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Setor setor = null;
		try {
			setor = em.find(Setor.class, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return setor;
	}
}
