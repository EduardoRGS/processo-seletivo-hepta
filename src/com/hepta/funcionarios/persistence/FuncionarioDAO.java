package com.hepta.funcionarios.persistence;

import java.util.List;

import javax.persistence.EntityManager;

import com.hepta.funcionarios.entity.Funcionario;

public class FuncionarioDAO {

	public void save(Funcionario funcionario) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		
		try {
			em.getTransaction().begin();
			em.persist(funcionario);
			System.out.println("Teste cadastra: " + funcionario);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	public Funcionario update(Funcionario funcionario, Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();

		Funcionario funcionarioAtualizado = em.find(Funcionario.class, id);
		funcionarioAtualizado.setNome(funcionario.getNome());
		funcionarioAtualizado.setSetor(funcionario.getSetor());
		funcionarioAtualizado.setSalario(funcionario.getSalario());
		funcionarioAtualizado.setEmail(funcionario.getEmail());
		funcionarioAtualizado.setIdade(funcionario.getIdade());

		try {
			em.getTransaction().begin();
			em.merge(funcionarioAtualizado);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return funcionarioAtualizado;
	}

	public void delete(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			Funcionario funcionario = em.find(Funcionario.class, id);
			em.remove(funcionario);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	public Funcionario find(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Funcionario Funcionario = null;
		try {
			Funcionario = em.find(Funcionario.class, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return Funcionario;
	}

	public List<Funcionario> getAll() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			String query = "SELECT f FROM Funcionario f";
			List<Funcionario> funcionarios =  em.createQuery(query, Funcionario.class).getResultList();
			return funcionarios;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

}
