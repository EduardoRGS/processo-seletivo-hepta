package com.hepta.funcionarios.test;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.HibernateUtil;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FuncionarioServiceTest {


    private final static EntityManager em = HibernateUtil.getEntityManager();

    // Ao rodar todos os testes de uma vez, verificar se os id´s do métodos de teste são iguais do banco da dados.

    @Test
    @BeforeEach
    public void testSetorCreate() {
        Setor setor = new Setor();
        setor.setNome("Setor Teste");

        em.getTransaction().begin();
        em.persist(setor);
        em.getTransaction().commit();
    }

    @Test
    public void testFuncionarioCreate() {

        Funcionario funcionario = new Funcionario();
        Setor setor = new Setor();

        setor.setId(7);

        funcionario.setNome("Eduardo");
        funcionario.setSalario(3000.00);
        funcionario.setSetor(setor);
        funcionario.setEmail("teste@teste.com");
        funcionario.setIdade(21);

        em.getTransaction().begin();

        em.persist(funcionario);
        em.getTransaction().commit();

    }

    @Test
    void testFuncionarioRead() {
        String query = "SELECT f FROM Funcionario f";
        List<Funcionario> funcionarios =  em.createQuery(query, Funcionario.class).getResultList();
        Assertions.assertFalse(((List<?>) funcionarios).isEmpty());
    }

    @Test
    void testFuncionarioFind() {
        Integer id = 26;
        Funcionario funcionario = null;
        funcionario = em.find(Funcionario.class, id);
        assertEquals("Eduardo", funcionario.getNome());
    }

    @Test
    void testFuncionarioUpdate() {
        Integer id = 36;
        Funcionario funcionarioAtualizado = new Funcionario();
        Setor setor = new Setor();

        setor.setId(7);

        funcionarioAtualizado.setId(id);
        funcionarioAtualizado.setNome("Eduardo 2");
        funcionarioAtualizado.setSalario(2000.00);
        funcionarioAtualizado.setSetor(setor);
        funcionarioAtualizado.setEmail("teste2@teste2.com");
        funcionarioAtualizado.setIdade(22);

        em.getTransaction().begin();
        em.merge(funcionarioAtualizado);
        em.getTransaction().commit();

        funcionarioAtualizado = em.find(Funcionario.class, id);

        assertEquals("Eduardo 2", funcionarioAtualizado.getNome());
    }

    @Test
    void testFuncionarioDelete() {
        Integer id = 47;
        Funcionario funcionario = em.find(Funcionario.class, id);
        em.getTransaction().begin();
        em.remove(funcionario);
        em.getTransaction().commit();

        Funcionario funcionarioDeletado = em.find(Funcionario.class, id);

        Assertions.assertNull(funcionarioDeletado);
    }
}
