package com.hepta.funcionarios.test;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.HibernateUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SetorServiceTest {

    private final static EntityManager em = HibernateUtil.getEntityManager();

    // Ao rodar todos os testes de uma vez, verificar se os id´s do métodos de teste são iguais do banco da dados.

    @Test
    @BeforeEach
    public void testSetorCreate() {
        Setor setor = new Setor();
        setor.setNome("Setor Teste");

        em.getTransaction().begin();
        em.persist(setor);
        em.getTransaction().commit();
    }

    @Test
    void testSetorRead() {
        String query = "SELECT s FROM Setor s";
        List<Setor> setores =  em.createQuery(query, Setor.class).getResultList();
        Assertions.assertFalse(((List<?>) setores).isEmpty());
    }

    @Test
    void testSetorFind() {
        Integer id = 1;
        Setor setor = null;
        setor = em.find(Setor.class, id);
        assertEquals("Setor Teste", setor.getNome());
    }

    @Test
    void testSetorUpdate() {
        Integer id = 2;
        Setor setorAtualizado = new Setor();

        setorAtualizado.setNome("Junit Setor");

        em.getTransaction().begin();
        em.merge(setorAtualizado);
        em.getTransaction().commit();

        setorAtualizado = em.find(Setor.class, id);
        assertEquals("Setor Teste", setorAtualizado.getNome());
    }

    @Test
    void testSetorDelete() {
        Integer id = 1;
        Setor setor = em.find(Setor.class, id);
        em.getTransaction().begin();
        em.remove(setor);
        em.getTransaction().commit();

        Setor setorDeletado = em.find(Setor.class, id);

        Assertions.assertNull(setorDeletado);
    }

}
